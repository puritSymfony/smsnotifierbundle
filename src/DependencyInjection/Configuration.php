<?php

namespace Bstd\HumanVerifierBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('bstd_human_verifier');

        $rootNode
            ->children()
            ->booleanNode('is_enabled')->defaultTrue()->end()
            ->scalarNode('sms_from')->isRequired()->end()
            ->scalarNode('sms_login')->isRequired()->end()
            ->scalarNode('sms_password')->isRequired()->end()
            ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
