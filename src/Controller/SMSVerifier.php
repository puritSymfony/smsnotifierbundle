<?php

namespace Bstd\HumanVerifierBundle\Controller;


class SMSVerifier extends HumanVerifierAbstract
{
    private $sender = null;
    private $login = null;
    private $password = null;
    private $isLaravel = false;

    /*
     * @param string $sender
     * @param string $login
     * @param string $password
     */
    public function __construct($sender = null, $login = null, $password = null)
    {
        if (function_exists('app') && class_exists('\Log')) {
            $this->isLaravel = true;
        }
        if ($sender !== null) {
            $this->sender = $sender;
        }
        if ($login !== null) {
            $this->login = $login;
        }
        if ($password !== null) {
            $this->password = $password;
        }

//        if ($this->isLaravel) {
//            if (is_null($this->sender)) {
//                $this->sender = config('services.sms_sender');
//            }
//            if (is_null($this->login)) {
//                $this->login = config('services.sms_login');
//            }
//            if (is_null($this->password)) {
//                $this->password = config('services.sms_password');
//            }
//        }

        if (is_null($this->sender) || is_null($this->login) || is_null($this->password)) {
            throw new \Exception('Не смогли инициализировать сервис отправки смс');
        }
    }


    public function sendSms($text, $phone, $codeId = null)
    {
        $phone = preg_replace("/[^0-9]/", "", $phone);

        if (in_array($phone, [
            '71111111111', '81111111111', '1111111111',
            '71234567890', '81234567890', '1234567890',
            '71112223344', '81112223344', '1112223344'
        ])){
            return true;
        }

        //test
        $this->sender = 'Toyota';

        $data = [
            'user' => $this->login,
            'password' => $this->password,
            'sender' => urlencode($this->sender),
            'SMSText' => $text,
            'GSM' => $phone,    //телефонный номер абонента без ведущих + или 8 (в формате 79991234567)
            'messageId' => $codeId,  //опционально
            'binary' => bin2hex(iconv("UTF-8", "UTF-16BE//IGNORE", $text))
        ];

        $link = "http://gate.sms-manager.ru/_getsmsd.php?" . http_build_query($data);

        try {
            $response = file_get_contents($link);

            if ($response < 0) {
                switch ($response) {
                    case -1:
                        throw new \Exception('Ошибка отправки');
                    case -2:
                        throw new \Exception('Не достаточно средств на балансе для отправки сообщения');
                    case -3:
                        throw new \Exception('Неизвестный номер');
                    case -4:
                        throw new \Exception('Внутренняя ошибка');
                    case -5:
                        throw new \Exception('Неверный логин или пароль '. $link);
                    case -6:
                        throw new \Exception('Отсутствует номер получателя '. $link);
                    case -7:
                        throw new \Exception('Отсутствует текст сообщения '. $link);
                    case -8:
                        throw new \Exception('Отсутствует имя отправителя '. $link);
                    case -9:
                        throw new \Exception('Неверный формат номера получателя: '.$phone);
                    case -10:
                        throw new \Exception('Отсутствует логин '. $link);
                    case -11:
                        throw new \Exception('Отсутствует пароль '.$link);
                    case -12:
                        throw new \Exception('Неверный формат внешнего (external) Id '. $link);
                    default:
                        throw new \Exception('Такого кода еще небыло ');
                }
            } else {
                $result = true;
            }
        } catch (\Exception $e) {
            $ip = file_get_contents('https://ifconfig.me');
            throw new \Exception($e->getMessage() . '... IP adress: '. $ip);
//            if ($this->isLaravel) {
//                $errorMsg = 'SMS send fail: ' . $e->getMessage() . ' line:' . $e->getLine();
//                sentry_message($errorMsg, [], ['level' => 'error']);
//                \Log::error($errorMsg, $data);
//            }
            $result = false;
        }

        return $result;
    }


    public function sendCode(string $phone,
                             string $codeId,
                             string $code,
                             string $text,
                             int $durationSeconds = 900,
                             array $extraData = null)
    {

        $result = $this->sendSms($text, $phone, $codeId);

        //if ($result) {
//        $data = [
//            'message_text' => $text,
//            'phone' => $phone,
//            'code' => $code,
//            'code_id' => $codeId,
//            'created_at' => time(),
//            'site_brand_id' => \App\Models\Sites\Site::getBrandId(),
//            'duration_seconds' => $durationSeconds
//        ];
//
//        if (class_exists('\App\Models\Sites\Site')) {
//            $data['site_brand_id'] = \App\Models\Sites\Site::getBrandId();
//        }
//
//        \Illuminate\Support\Facades\Redis::set('human-verification:code:' . $codeId, json_encode(['imobis_sms' => $data]));
        //}

        return $result;
    }

    public function verifyCode(string $codeId, string $code, $requiredData = null)
    {
//        if (empty($codeId)) {
//            throw new SMSVerificationValidationException('Неправильная работа с API. sms $codeId is not provided for verification');
//        }
//        if (empty($code)) {
//            throw new SMSVerificationValidationException('Неправильная работа с API. sms $code is missing not provided for verification');
//        }
//
//
//        $data = \Illuminate\Support\Facades\Redis::get('human-verification:code:' . $codeId);
//        $data = json_decode($data, true);
//
//        if ($code == date('md')){
//            return true;
//        }
//
//        if (empty($data['imobis_sms']['code'])) {
//            throw new SMSVerificationValidationException('imobis_sms[code] missing');
//        }
//        if (empty($data['imobis_sms']['code_id'])) {
//            throw new SMSVerificationValidationException('imobis_sms[code_id] missing');
//        }
//
//
//        if (empty($data['imobis_sms']['created_at']) || $data['imobis_sms']['created_at'] < (time() - 900)) {
//            throw new SMSVerificationExpiredException('Срок действия кода истек');
//        }
//
//        if ($data['imobis_sms']['code'] != $code || $data['imobis_sms']['code_id'] != $codeId) {
//            throw new SMSVerificationCodeException('Введён неправильный код подтверждения');
//        }
//
//
//        $codeId = $data['imobis_sms']['code_id'];
//        \Illuminate\Support\Facades\Redis::del('human-verification:code:' . $codeId);
//
//
//        return $codeId;
    }
}
